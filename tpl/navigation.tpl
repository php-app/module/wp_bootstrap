{function navigation level=0}

	{foreach from=$D name=NAV item=NAV key=kNAV}
		{if $NAV.D}
		<li data-toggle="collapse" data-target="#nav_{$kNAV}" class="collapsed {*active*}">
			<a href="#"><i class="fa fa-dashboard fa-lg"></i>{$kNAV} <span class="arrow"></span></a>
		</li>
		<ul class="sub-menu collapse" id="nav_{$kNAV}">{navigation D=$NAV.D}</ul>
		
		{else}
		<li {if $NAV.LANGUAGE.D.DE.LINK}onclick="$('#page').load('?{$NAV.LANGUAGE.D.DE.LINK}'); return false;"{/if}>
			<a href="#">{$NAV.LANGUAGE.D.DE.TITLE}</a>
		</li>
		{/if}
	{/foreach}

{/function}

{*
{function navigation level=0}

	{foreach from=$D name=NAV item=NAV key=kNAV}
		{if $NAV.D}
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{$kNAV} <span class="caret"></span></a>
			<ul class="dropdown-menu">{navigation D=$NAV.D}</ul>
		</li>
		{else}
		<li>
			<a {if $NAV.LANGUAGE.D.DE.LINK}onclick="$('#page').load('?{$NAV.LANGUAGE.D.DE.LINK}'); return false;"{/if} href="#">{$NAV.LANGUAGE.D.DE.TITLE}</a>
		</li>
		{/if}
	{/foreach}

{/function}


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">{navigation D=$D.NAVIGATION.D.LEFT.D}</ul>
	  <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><pan class="glyphicon glyphicon-console"></span></a>
          <ul class="dropdown-menu">
				<li><a onclick="$('#page').load('?D[PAGE]=indexsetting'); return false;" href="#"><span class="glyphicon glyphicon-cog"></span> SETTING</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-th-large"></span> Module <span class="badge">14</span></a></li>
          </ul>
        </li>
      </ul>
	  
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
*}


<div class="nav-side-menu" style="position:fixed;float:left;z-index:10;">
	<div class="brand">Admin</div>
	<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
	<div class="menu-list">
		
		<ul id="menu-content" class="menu-content collapse out">
			{navigation D=$D.NAVIGATION.D.LEFT.D}
		</ul>
		{*
		<ul id="menu-content" class="menu-content collapse out">
			<li>
				<a href="#">
				<i class="fa fa-dashboard fa-lg"></i> Dashboard
				</a>
			</li>

			<li data-toggle="collapse" data-target="#products" class="collapsed active">
				<a href="#"><i class="fa fa-gift fa-lg"></i> UI Elements <span class="arrow"></span></a>
			</li>
			<ul class="sub-menu collapse" id="products">
				<li class="active"><a href="#">CSS3 Animation</a></li>
				<li><a href="#">General</a></li>
				<li><a href="#">Buttons</a></li>
				<li><a href="#">Tabs & Accordions</a></li>
				<li><a href="#">Typography</a></li>
				<li><a href="#">FontAwesome</a></li>
				<li><a href="#">Slider</a></li>
				<li><a href="#">Panels</a></li>
				<li><a href="#">Widgets</a></li>
				<li><a href="#">Bootstrap Model</a></li>
			</ul>


			<li data-toggle="collapse" data-target="#service" class="collapsed">
				<a href="#"><i class="fa fa-globe fa-lg"></i> Services <span class="arrow"></span></a>
				</li>  
				<ul class="sub-menu collapse" id="service">
				<li>New Service 1</li>
				<li>New Service 2</li>
				<li>New Service 3</li>
			</ul>


			<li data-toggle="collapse" data-target="#new" class="collapsed">
				<a href="#"><i class="fa fa-car fa-lg"></i> New <span class="arrow"></span></a>
				</li>
				<ul class="sub-menu collapse" id="new">
				<li>New New 1</li>
				<li>New New 2</li>
				<li>New New 3</li>
			</ul>


			<li>
				<a href="#">
				<i class="fa fa-user fa-lg"></i> Profile
				</a>
			</li>

			<li>
				<a href="#">
				<i class="fa fa-users fa-lg"></i> Users
				</a>
			</li>
		</ul>
*}
	</div>
			{*
				<ul id="menu-content" class="menu-content ">
					<li class="col-xs-2">
						<a href="#">
						<i class="fa fa-user fa-lg"></i> 
						</a>
					</li>
					<li class="col-xs-2">
						<a href="#">
						<i class="fa fa-user fa-lg"></i> 
						</a>
					</li>
				</ul>
			*}
</div>
