<?
class wp_bootstrap__class__library extends wp_bootstrap__class__library__parent
{
	function __construct(&$D = null)
	{
		parent::{__function__}($D);
		#$this->D['CL']->setting()->get_setting();
		
		$this->D['LIBRARY']['JS']['BOOTSTRAP']			= ['ACTIVE'	=> 1, 'REQUIRED' => 'JQUERY', 'FILE' => ['library/bootstrap/js/bootstrap.min.js']];
		$this->D['LIBRARY']['CSS']['BOOTSTRAP']			= ['ACTIVE'	=> 1, 'FILE' => ['library/bootstrap/css/bootstrap.min.css']];
		#$this->D['LIBRARY']['CSS']['BOOTSTRAP_THEME']	= ['ACTIVE'	=> 1, 'FILE' => ['library/bootstrap/css/bootstrap-theme.css']];
		$this->D['LIBRARY']['CSS']['FONT_AWESOME']		= ['ACTIVE'	=> 1, 'FILE' => ['library/bootstrap/css/font-awesome.min.css']];
	}
} 